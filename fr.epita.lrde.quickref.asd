;;; fr.epita.lrde.quickref.asd --- ASDF system definition

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:load-system :fr.epita.lrde.quickref.setup/parallel)

(asdf:defsystem :fr.epita.lrde.quickref
  :description "Reference Manuals for Quicklisp Libraries"
  :long-description "\
Quickref is a global documentation project for Common Lisp libraries.
It builds a website containing reference manuals for every Quicklisp[1]
library available. The reference manuals are generated by Declt[2] in
Texinfo[3] format, and then processed by makeinfo. The official Quickref
website[4] is kept up-to-date with Quicklisp.

[1] https://www.quicklisp.org
[2] https://github.com/didierverna/declt
[3] https://www.gnu.org/software/texinfo/
[4] http://quickref.common-lisp.net"
  :author "Antoine Martin"
  :maintainer ("Antoine Martin" "Didier Verna")
  :mailto "quickref-devel@common-lisp.net"
  :license "BSD"
  :version #.(fr.epita.lrde.quickref.setup:version :short)
  :depends-on (:fr.epita.lrde.quickref.core
	       (:feature :fr.epita.lrde.quickref.parallel
		:fr.epita.lrde.quickref.parallel)))

;;; fr.epita.lrde.quickref.asd ends here
