;;; fr.epita.lrde.quickref.parallel.asd --- ASDF system definition, // feature

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:load-system :fr.epita.lrde.quickref.setup)

(asdf:defsystem :fr.epita.lrde.quickref.parallel
  :long-name "Quickref Parallel"
  :description "Quickref's parallel library"
  :long-description "\
Quickref's parallel library provides various parallel implementations of the
BUILD function. For a more complete description of Quickref, see the
fr.epita.lrde.quickref system."
  :author "Didier Verna"
  :maintainer ("Antoine Martin" "Didier Verna")
  :mailto "quickref-devel@common-lisp.net"
  :license "BSD"
  :version #.(fr.epita.lrde.quickref.setup:version :short)
  :defsystem-depends-on (:fr.epita.lrde.quickref.setup/parallel)
  :if-feature (:and :sb-thread :fr.epita.lrde.quickref.parallel)
  :depends-on (:fr.epita.lrde.quickref.core)
  :serial t
  :components ((:file "parallel")))

;;; fr.epita.lrde.quickref.core.asd ends here
