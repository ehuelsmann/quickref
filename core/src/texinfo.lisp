;;; texinfo.lisp --- Texinfo generation part

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


(defvar *declt-script* (merge-pathnames #p"declt" *bin-directory*)
  "Quickref's Declt script.")


(defun run-declt (system-name)
  "Run Declt on ASDF SYSTEM-NAME. Return T on success."
  (let ((log-errors (eq *error-behavior* :log)))
    (multiple-value-bind (output error-output status)
	(uiop:run-program
	 `(,sb-ext:*runtime-pathname*
	   "--script" ,(namestring *declt-script*)
	   ,system-name
	   "--quicklisp-setup" ,(namestring (ql-setup:qmerge "setup.lisp"))
	   ,@(unless (eq *cache-policy* :global)
	       `("--cache-directory" ,(namestring (cache-directory))))
	   "--texinfo-directory" ,(namestring (texinfo-directory)))
	 :output (if log-errors nil *standard-output*)
	 :error-output (if log-errors :string *error-output*)
	 :ignore-error-status log-errors)
      (declare (ignore output))
      (unless (zerop status)
	(with-open-file (log (declt-log-file system-name)
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create
			 :external-format :utf-8)
	  (format log "~A~%" error-output)))
      (zerop status))))

#+()(defgeneric build-texinfo-manual (library)
  (:documentation "Build LIBRARY's Texinfo manual. Return T on success.")
  (:method ((system-name string))
    "Build ASDF SYSTEM-NAME's Texinfo manual. Return T on success."
    (with-progression ("Generating ~A.texi" system-name)
      (run-declt system-name)))
  (:method ((system ql-dist:system))
    "Build Quicklisp SYSTEM's Texinfo manual. Return T on success."
    (build-texinfo-manual (ql-dist:name system)))
  (:method ((release ql-dist:release))
    "Build Quicklisp RELEASE's Texinfo manual. Return T on success."
    (build-texinfo-manual (primary-system release))))

#+()(defun build-texinfo-manuals (libraries)
  "Build LIBRARIES Texinfo manuals."
  (format t "~A Texinfo manuals to generate.~%" (length libraries))
  (mapc #'build-texinfo-manual libraries)
  (values))

;;; texinfo.lisp ends here
