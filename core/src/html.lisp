;;; html.lisp --- HTML generation part

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; Miscellaneous Utilities
;; ==========================================================================

(defun copy-style-sheets ()
  "Copy Quickref's style sheets to the HTML output directory."
  (format t "Copying stylesheets...~%")
  (let ((style-sheets '(#p"document.css" #p"index.css")))
    (dolist (style-sheet style-sheets)
      (uiop:copy-file (merge-pathnames style-sheet *css-directory*)
		      (merge-pathnames style-sheet (html-directory))))))



;; ==========================================================================
;; Index Files
;; ==========================================================================

(defvar *indexes* '("library" "author") "The list of HTML indexes.")

(defun index-file-name (kind)
  "Return \"index-per-KIND\"."
  (concatenate 'string "index-per-" kind))


;; --------------------
;; Shared functionality
;; --------------------

(defun render-index-header (manuals-number index-name)
  "Render index file's header to standard output.
The header also advertise MANUALS-NUMBER and INDEX-NAME."
  (format t (file-contents (merge-pathnames #p"index-header.html"
					    *templates-directory*))
    (ql-dist:version (quicklisp-distribution))
    (version :long)
    (net.didierverna.declt:version :long)
    manuals-number
    index-name))

(defun column-lengths (number)
  "Spread NUMBER of entries in 3 different columns as uniformly as possible.
Return the column lengths as 3 values."
  (multiple-value-bind (quotient remainder) (floor number 3)
    (case remainder
      (0 (values quotient quotient quotient))
      (1 (values (+ quotient 1) quotient quotient))
      (2 (values (+ quotient 1) (+ quotient 1) quotient)))))

(defun render-index-entry (character entries number renderer)
  "Render index file's first NUMBER ENTRIES under CHARACTER.
This function arranges ENTRIES to be rendered in 3 columns, vertically from
left to right, but taking care of vertical justification as much as possible:
the heights of the 3 columns may only differ by one, meaning that only the
last row may have less than 3 entries.

RENDERER is the function used to render the entries.
Rendering is done on *STANDARD-OUTPUT*."
  (format t "~%      <tr><th><a name=\"~A\">~A</a></th></tr>~%"
    (if (char= character #\#) "other" character)
    character)
  (multiple-value-bind (l1 l2 l3) (column-lengths number)
    (loop :for entries-1 :on entries
	  :for entries-2 :on (nthcdr l1 entries)
	  :for entries-3 :on (nthcdr (+ l1 l2) entries)
	  ;; #### WARNING: do this last so that the manual names pointers are
	  ;; correct in the :FINALLY clause, and even if the :DO clause is not
	  ;; executed.
	  :for lines :from 1 :upto l3
	  ;; #### FIXME: this call still be improved with FORMAT list
	  ;; arguments for multiple cells
	  :do (progn
		(format t "      <tr>~%")
		(funcall renderer (car entries-1))
		(funcall renderer (car entries-2))
		(funcall renderer (car entries-3))
		(format t "      </tr>~%"))
	  :finally (cond ((> l1 l2)
			  (format t "      <tr>~%")
			  (funcall renderer (car entries-1))
			  (format t "      </tr>~%"))
			 ((> l2 l3)
			  (format t "      <tr>~%")
			  (funcall renderer (car entries-1))
			  (funcall renderer (car entries-2))
			  (format t "      </tr>~%")))))
  (values))


;; -------------
;; Library Index
;; -------------

(defun library-index-character (entries)
  "Return the next library index character for ENTRIES.
ENTRIES should be a list of system names.
The next index character is the first (upcased) letter of the first system
name in ENTRIES trimmed if it's alphabetic, or # otherwise.
This function is used as the INDEX-CHARACTER-GETTER argument to
BUILD-INDEX-FILE."
  (when entries
    (let ((char (aref (trim (first entries)) 0)))
      (if (alpha-char-p char) (char-upcase char) #\#))))

(defun render-library-index-entry (entry)
  "Render a library index ENTRY.
ENTRY should be a manual name. Rendering is done on *STANDARD-OUTPUT*.
This function is used as the RENDERER argument to RENDER-INDEX-ENTRY."
  (format t "	<td></td><td><a href=\"~A.html\">~A</a></td>~%" entry entry))

(defun library-index-entries (manual-names)
  "Return a sorted list of MANUAL-NAMES for the library index.
Sorting is done on trimmed names."
  (sort manual-names #'string< :key #'trim))


;; ------------
;; Author Index
;; ------------

;; #### WARNING: the author index generation, as currently implemented,
;; #### doesn't scale well. It requires the main thread to call
;; #### ASDF:FIND-SYSTEM on every documented system (so potentially the whole
;; #### Quicklisp world). This is heavy and I've seen problems where
;; #### FIND-SYSTEM fails in some conditions, and succeeds in others (UCW is
;; #### one example). Besides, some libraries perform side-effects in
;; #### FIND-SYSTEM, including printing stuff on standard output, so we have
;; #### to be careful and those problems will likely never stop coming in.

(defun author-index-character (entries)
  "Return the next author index character for ENTRIES.
ENTRIES should be a list of lists of the form (AUTHOR-NAME SYSTEM-NAME...).
The next index character is the first (upcased) letter of the first author
name if it's alphabetic, or # otherwise.
This function is used as the INDEX-CHARACTER-GETTER argument to
BUILD-INDEX-FILE."
  (when entries
    (let ((char (aref (car (first entries)) 0)))
      (if (alpha-char-p char) (char-upcase char) #\#))))

(defun render-author-index-entry (entry)
  "Render an author index ENTRY.
ENTRY should be a list of the form (AUTHOR-NAME SYSTEM-NAME...).
Rendering is done on *STANDARD-OUTPUT*.
This function is used as the RENDERER argument to RENDER-INDEX-ENTRY."
  (format t "        <td></td>
	<td class=\"author-entry\">
	  <table>
	    <tr><td class=\"author-name\">~A</td></tr>
~{            <tr><td><a href=\"~a.html\">~:*~a</a></td></tr>~%~}	  </table>
	</td>~%"
    (car entry) (cdr entry)))

;; #### FIXME: this is a duplication of what DECLT does. I need to update
;; DECLT to abstract this away, so that it can be re-used. -- didier
(defun system-authors
    (system-name
     ;; #### FIXME: this gross hack needs to go. It is impossible to find all
     ;; Quicklisp systems in the same Lisp image without getting errors. Just
     ;; ignoring them is a quick workaround to still be able to generate a
     ;; decent author index, but not a long term solution. See the warning
     ;; above.
     &aux (system (ignore-errors (asdf:find-system system-name)))
	  (contact (when system (asdf:system-author system))))
  "Return ASDF SYSTEM's authors and maintainers."
  (when (stringp contact) (setq contact (list contact)))
  (when system
    (cond ((stringp (asdf:system-maintainer system))
	   (push (asdf:system-maintainer system) contact))
	  ((consp (asdf:system-maintainer system))
	   (setq contact (append (asdf:system-maintainer system) contact)))))
  (unless contact (setq contact (list "John Doe")))
  (setq contact (remove-duplicates contact :from-end t :test #'string=))
  ;; #### NOTE: this function may return empty strings. Declt doesn't have a
  ;; problem with this, but we do, so we need to filter those out.
  (delete-if (lambda (author) (zerop (length author)))
	     (net.didierverna.declt::|parse-contact(s)| contact)))

(defun author-index-entries
    (manual-names &aux (hash (make-hash-table :test 'equal)))
  "Return a list of author index entries from MANUAL-NAMES.
Each element is a list of the form (AUTHOR-NAME SYSTEM-NAME...).
SYSTEM-NAMEs are sorted by lexicographic order.
The list is sorted by lexicographic order of AUTHOR-NAMES."
  (loop :for system-name :in manual-names
	:do (dolist (author (system-authors system-name))
	      (setf (gethash author hash)
		    (pushnew system-name (gethash author hash)
			     :test #'string=))))
  (sort (loop :for author :being :the :hash-keys :in hash
		:using (:hash-value system-names)
	      ;; #### NOTE: whether we want to sort manuals on trimmed names
	      ;; here is questionable. The lists in the author index are not
	      ;; that long after all.
	      :collect (cons author (sort system-names #'string< :key #'trim)))
	#'string< :key #'car))


;; -----------
;; Entry point
;; -----------

(defun build-index-file
    (kind manual-names
     ;; #### NOTE: ENTRIES was originally a LOOP local variable, which causes
     ;; problems with the current implementation of the author index, because
     ;; at that time, *STANDARD-OUTPUT* is already redirected to the index
     ;; file (see comment at the top of the author index code.
     &aux (entries (funcall (intern (format nil "~:@(~A~)-INDEX-ENTRIES" kind)
				    :fr.epita.lrde.quickref)
		     manual-names))
	  (index-character-getter
	   (intern (format nil "~:@(~A~)-INDEX-CHARACTER" kind)
		   :fr.epita.lrde.quickref))
	  (index-entry-renderer
	   (intern (format nil "RENDER-~:@(~A~)-INDEX-ENTRY" kind)
		   :fr.epita.lrde.quickref)))
  "Build Quickref's KIND index file for MANUAL-NAMES.
The index file is called \"index-per-KIND.html\"."
  (format t "Building ~A index file...~%" kind)
  (with-open-file (*standard-output* (html-file (index-file-name kind))
		   :direction :output
		   :if-exists :supersede
		   :if-does-not-exist :create)
    (loop :with index-character := (funcall index-character-getter entries)
	  :with next-entries := (cdr entries)
	  :with length := 1
	  :initially (render-index-header
		      (length manual-names) (format nil "~:(~A index~)" kind))
	  :while entries
	  :if (and next-entries
		   (char= index-character
			  (funcall index-character-getter next-entries)))
	    :do (setq length (1+ length) next-entries (cdr next-entries))
	  :else
	    :do (progn
		  (render-index-entry index-character entries length
				      index-entry-renderer)
		  (setq length 1
			entries next-entries
			index-character (funcall index-character-getter entries)
			next-entries (cdr entries))))
    (format t "    </table>~%  </body>~%</html>~%")))

;; #### FIXME: at some point, and especially if we're going to document
;; several primary systems per release, the index generation will need to stop
;; relying on the directory's contents, and instead, depend on information
;; collected during the building process (all the more that the author index
;; needs information that can only retrieved by actually loading the systems,
;; which is the job of the Declt external processes).
(defun build-index-files
    (&aux (manual-names
	   (let ((index-file-names
		   (cons "index" (mapcar #'index-file-name *indexes*))))
	     (remove-if
	      (lambda (name)
		(member name index-file-names :test #'string=))
	      (mapcar #'pathname-name (*.html (html-directory)))))))
  "Build Quickref's index files."
  ;; #### WARNING: this is the same hack as in Declt, but for a different
  ;; reason. Without this, calling SYSTEM-AUTHOR on UIOP triggers an error
  ;; related to virtual slots access (that I wrote). This should probably be
  ;; investigated and fixed somehow.
  (load (merge-pathnames "uiop/uiop.asd"
			 (asdf:system-source-directory
			  (asdf:find-system :asdf))))
  (dolist (index *indexes*) (build-index-file index manual-names))
  (with-open-file (*standard-output* (html-file "index")
		   :direction :output
		   :if-exists :supersede
		   :if-does-not-exist :create)
    (format t "~
<meta http-equiv=\"refresh\" content=\"0;url=index-per-library.html\">~%")))



;; ==========================================================================
;; HTML Files
;; ==========================================================================

;; -----------------
;; README Management
;; -----------------

(defun markdown-html (string)
  "Return Markdown STRING formatted as HTML.
Try our CommonMark FFI first, fall back to 3BMD otherwise."
  (handler-case (cmark-markdown-to-html string (length string) 0)
    (error ()
      (with-output-to-string (out)
	(3bmd:parse-string-and-print-to-stream string out)))))

(defun <pre> (string)
  "Return STRING as html PRE."
  (format nil "<pre style=\"white-space: pre-wrap;\">~%~A~%</pre>" string))

(defun readme-html (readme)
  "Return contents of README file formatted as HTML."
  (let ((contents (file-contents readme)))
    (if (member (string-downcase (pathname-type readme))
		'("md" "markdown" "mdown" "mkdn" "mdwn" "mkd")
		:test #'string=)
      (markdown-html contents)
      (<pre> contents))))

(defun insert-readme-contents (html-file readme-file)
  "Insert the contents of README-FILE into HTML-FILE.
README contents is rendered in HTML and inserted as introduction."
  (let ((html-contents (file-contents html-file)))
    (with-open-file
	(*standard-output* html-file
	 :direction :output :if-exists :supersede :external-format :utf-8)
      (write-string
       (cl-ppcre:regex-replace
	"\\<h2 class=\\\"chapter\\\"\\>1 Introduction\\<\\/h2\\>\\n"
	html-contents
	;; #### FIXME: we're gonna have problems if the README file is not in
	;; UTF8...
	(list :match (readme-html readme-file)))))))


;; ---------------
;; HTML Management
;; ---------------

;; #### FIXME: it would be better if this function returned the HTML file on
;; success.
(defun run-makeinfo (system-name)
  "Run Makeinfo on ASDF SYSTEM-NAME. Return T on success."
  (let* ((html-file (html-file system-name))
	 (log-errors (eq *error-behavior* :log)))
    (multiple-value-bind (output error-output status)
	(uiop:run-program
	 `(,*makeinfo-path*
	   "-o" ,(namestring html-file)
	   "--html"
	   "--css-ref" "/document.css"
	   "-c" "TOP_NODE_UP_URL=/"
	   "--no-split"
	   ,@(unless *cross-reference-validation* '("--no-validate"))
	   ,(namestring (texinfo-file system-name)))
	 :output (if log-errors nil *standard-output*)
	 :error-output (if log-errors :string *error-output*)
	 :ignore-error-status log-errors)
      (declare (ignore output))
      (unless (zerop status)
	;; #### NOTE: if we reach that point (that is, without an error
	;; signaled, it means that we're logging errors.
	(with-open-file (log (makeinfo-log-file system-name)
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create
			 :external-format :utf-8)
	  (format log "~A~%" error-output)))
      (zerop status))))

#+()(defgeneric build-html-manual (library)
  (:documentation "Build LIBRARY's HTML manual. Return T on success.")
  (:method ((system-name string))
    "Build ASDF SYSTEM-NAME's HTML manual. Return T on success."
    ;; #### WARNING: this asymmetry is not nice, but it will go away once
    ;; Declt itself is capable of README insertion.
    (let ((result (with-progression ("Generating ~A.html" system-name)
		    (run-makeinfo system-name))))
      (when result
	(let ((readme-file (readme-file system-name)))
	  (when readme-file
	    (with-progression ("Inserting README file")
	      (insert-readme-contents (html-file system-name) readme-file))))
	t)))
  (:method ((system ql-dist:system))
    "Build Quicklisp SYSTEM's HTML manual. Return T on success."
    (build-html-manual (ql-dist:name system)))
  (:method ((release ql-dist:release))
    "Build Quicklisp RELEASE's HTML manual. Return T on success."
    (build-html-manual (primary-system release))))

#+()(defun build-html-manuals ()
  "Build all reference manuals in HTML with Makeinfo."
  (mapc #'build-html-manual (*.* (texinfo-directory)))
  (values))

;;; html.lisp ends here
