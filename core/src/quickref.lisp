;;; quickref.lisp --- Entry points

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Comment:



;;; Code:

(in-package :fr.epita.lrde.quickref)


(defgeneric build-manual (library)
  (:documentation "Build LIBRARY's reference manual.
Return :html, :texi or NIL, indicating how far the process went.")
  (:method ((system-name string))
    "Build ASDF SYSTEM-NAME's reference manual.
Return :html, :texi or NIL, indicating how far the process went."
    (format t "Building ~A manual...~%" system-name)
    ;; #### WARNING: this asymmetry is not nice, but it will go away once
    ;; Declt itself is capable of README insertion.
    (when (with-progression ("  Running Declt") (run-declt system-name))
      (cond ((with-progression ("  Running Makeinfo")
	       (run-makeinfo system-name))
	     (let ((readme-file (readme-file system-name)))
	       (when readme-file
		 (with-progression ("  Inserting README file")
		   (insert-readme-contents (html-file system-name)
					   readme-file))))
	     :html)
	    (t :texi))))
  (:method ((system ql-dist:system))
    "Build Quicklisp SYSTEM's reference manual.
Return :html, :texi or NIL, indicating how far the process went."
    (build-manual (ql-dist:name system)))
  (:method ((release ql-dist:release))
    "Build Quicklisp RELEASE's reference manual.
Return :html, :texi or NIL, indicating how far the process went."
    (build-manual (primary-system release))))

(defun build-manuals (libraries)
  "Build LIBRARIES reference manuals."
  (loop :with total := (length libraries)
	:and htmls := 0
	:and texis := 0
	:for count :from 1
	:for library :in libraries
	:do (case (progn (format t "(~D/~D) " count total)
			 (build-manual library))
	      (:texi (incf texis))
	      (:html (incf texis) (incf htmls)))
	:finally (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
		   htmls total (- total texis) (- texis htmls)))
  (values))

(defvar *other-systems-names* '("asdf" "fr.epita.lrde.quickref")
  "Non-Quicklisp systems that need to be documented.")

(defun build (&key (update *update*)
		   (libraries *libraries*)
		   (output-directory *output-directory*)
		   (makeinfo-path *makeinfo-path*)
		   (cross-reference-validation *cross-reference-validation*)
		   (error-behavior *error-behavior*)
		   (cache-policy *cache-policy*)
		   (empty-cache *empty-cache*)
		   (parallel *parallel*)
		   (declt-threads *declt-threads*)
		   (makeinfo-threads *makeinfo-threads*))
  "Build the Quickref website.
All keywords to this function are defaulted to their corresponding global
parameter, which see."
  (when update
    (ql:update-client :prompt nil)
    (ql:update-dist (quicklisp-distribution) :prompt nil))
  (setq libraries (if (eq libraries :all)
		    (ql-dist:provided-releases (quicklisp-distribution))
		    (ql-dist:installed-releases (quicklisp-distribution))))
  (let ((*output-directory* output-directory)
	(*makeinfo-path* makeinfo-path)
	(*cross-reference-validation* cross-reference-validation)
	(*error-behavior* error-behavior)
	(*cache-policy* cache-policy)
	(*empty-cache* empty-cache))
    (ensure-quickref-directory :declt t :makeinfo t)
    (format t "~D foreign manual~:P to build.~%" (length *other-systems-names*))
    (build-manuals *other-systems-names*)
    (format t "~D Quicklisp manual~:P to build.~%" (length libraries))
    (ecase parallel
      ((nil) (build-manuals libraries))
      (1 (if (fboundp 'build-manuals-//-1)
	   (funcall #'build-manuals-//-1 libraries)
	   (error "Parallel support is unavailable.")))
      (2 (if (fboundp 'build-manuals-//-2)
	   (funcall #'build-manuals-//-2
	     libraries declt-threads makeinfo-threads)
	   (error "Parallel support is unavailable.")))
      (3 (if (fboundp 'build-manuals-//-3)
	   (funcall #'build-manuals-//-3
	     libraries declt-threads makeinfo-threads)
	   (error "Parallel support is unavailable.")))
      ((4 t) (if (fboundp 'build-manuals-//-4)
	       (funcall #'build-manuals-//-4
		 libraries declt-threads makeinfo-threads)
	       (error "Parallel support is unavailable."))))
    (build-index-files)
    (copy-style-sheets))
  (values))

;; quickref.lisp ends here
