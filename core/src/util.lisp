;;; util.lisp --- Miscellaneous utilities

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; User-Level Configuration
;; ==========================================================================

(defparameter *update* t
  "Whether to update Quicklisp before processing (T by default).")

(defparameter *libraries* :all
  "Quicklisp libraries to process (:all by default).
When :all, process (and hence download) all Quicklisp libraries. Otherwise,
only process the libraries that are already installed.")

(defparameter *output-directory*
  (merge-pathnames #p"quickref/" (user-homedir-pathname))
  "Quickref's output directory (~/quickref/ by default).")

(defparameter *makeinfo-path* #p"makeinfo"
  "Path to the 'makeinfo' executable.")

(defparameter *cross-reference-validation* t
  "Whether to validate cross references (T by default).
Validation help catch Declt bugs, but also prevent otherwise usable manuals
from being generated.") 

(defparameter *cache-policy* :global
  "Compilation cache handling policy (:global by default).
If :global, the usual ASDF cache is used, meaning that compilation caches
are shared across Declt calls. Otherwise, every Declt call uses a different
compilation cache.")

(defparameter *empty-cache* t
  "Whether to empty the compilation cache before proceeding.
This parameter is only considered for a local cache policy. The global ASDF
cache is never emptied.")

(defparameter *error-behavior* :log
  "Behavior to adopt when errors are encountered (:log by default).
If :log, just log the errors and continue processing. Otherwise, stop at the
first error. Note also that when errors are logged (which is meant to be the
non-interactive way of running Quickref), normal output from Declt and
Makeinfo is discarded completely.")

;; #### NOTE: the question of whether these should be defined in the parallel
;; module may be raised. This would complicate the code in the BUILD function
;; however.
(defparameter *parallel* nil
  "Whether to parallelize the build process (NIL by default).
If not null, the value must be the parallel version number to use, or T.
4 parallel algorithms are currently implemented (see the file parallel.lisp
for details). The most general solution is #4, selected by both 4 and T.")

(defparameter *declt-threads* 2
  "Number of threads to use for Declt (Texinfo generation).")

(defparameter *makeinfo-threads* 2
  "Number of threads to use for Makeinfo (HTML generation).")



;; ==========================================================================
;; Miscellaneous Utilities
;; ==========================================================================

(defmacro while (test &rest body)
  "The WHILE loop."
  `(do () ((not ,test))
     ,@body))

(defmacro endpush (object place)
  "Like PUSH, but at the end."
  `(setf ,place (nconc ,place (list ,object))))

(defgeneric trim (object)
  (:documentation "Remove potential CL- prefix from OBJECT's name.")
  (:method ((string string))
    "Remove potential CL- prefix from STRING."
    (if (and (> (length string) 3) (string= (subseq string 0 3) "cl-"))
      (subseq string 3)
      string))
  (:method ((system ql-dist:system))
    "Remove potential CL- prefix from SYSTEM's name."
    (trim (ql-dist:name system)))
  (:method ((release ql-dist:release))
    "Remove potential CL- prefix from RELEASE's name."
    (trim (ql-dist:name release))))

(defmacro with-progression ((message &rest arguments) &body body)
  "Execute BODY with a progression indicator. Return BODY's value.
MESSAGE and ARGUMENTS are passed to FORMAT to print the progression.
If Quickref errors are logged, the indicator is then followed by an elision,
eventually terminated by either ok or ko upon completion."
  (let ((log (gensym "log"))
	(result (gensym "result")))
    `(let ((,log (eq *error-behavior* :log))
	   ,result)
       (format t ,(concatenate 'string message "~:[~%~;... ~]")
	 ,@arguments ,log)
       (finish-output)
       (setq ,result (multiple-value-list (progn ,@body)))
       (when ,log (format t "~:[ko~;ok~]~%" (car ,result)))
       (values-list ,result))))



;; ==========================================================================
;; Quicklisp Utilities
;; ==========================================================================

;; #### NOTE: I find Quicklisp's object-orientation quite weird. Many objects
;; are created on demand, with slots sometimes unbound and methods on
;; SLOT-UNBOUND returning values instead (not even updating the slot in
;; question). As a result, there may be many different objects lying around at
;; the same time, yet representing the same thing in different
;; states. Example: FIND-DIST always returns a new object when you call it,
;; instead of consistently returning a single object per distribution. As a
;; result, the objects that you get aren't updated when the distribution is,
;; and e.g., you may end up with a wrong release date. That's why we use a
;; function instead of a variable below.
(defun quicklisp-distribution ()
  "Return a Quicklisp distribution object for the \"quicklisp\" distribution."
  (ql-dist:find-dist "quicklisp"))

(defun readme-file (system-name)
  "Return the pathname of a README(.*) file for ASDF SYSTEM-NAME, if any.
Try the system's directory first. If that fails and the system is part of a
Quicklisp RELEASE, also try the release's directory (which may be different)."
  (first
   (or (multiple-value-bind (foundp found-system pathname)
	   ;; #### NOTE: we don't use ASDF:SYSTEM-SOURCE-DIRECTORY here
	   ;; because it may fail when the system is missing some
	   ;; dependencies. Remember that we don't want anything else than the
	   ;; system's location here. Actual loading belongs to the Declt
	   ;; script.
	   (asdf:locate-system system-name)
	 (declare (ignore foundp found-system))
	 (when pathname
	   (directory (merge-pathnames "README.*"
				       (directory-namestring pathname)))))
       (let ((release (ql-dist:release (ql-dist:find-system-in-dist
					system-name
					(quicklisp-distribution)))))
	 (when release
	   (directory (merge-pathnames "README.*"
				       (ql-dist:base-directory release))))))))

;; #### FIXME: currently, we document one (primary) system per release. Most
;; of the time, it's the correct thing to do because:
;; - Declt automatically documents sub-systems as part of the primary one,
;; - we usually don't want to document secondary systems such as test ones.
;; In some situations, we will miss systems that we could document
;; however. For example, a library having a contrib directory may provide
;; additional and interesting primary systems. More importantly, some releases
;; provide a set of libraries (e.g. cl-gtk2 provides gtk, gdk, pango etc.)
;; that we should all document. It is impossible to guess all this
;; properly. What we could perhaps do is maintain a list of particular
;; releases for which we explicitly say which systems are primary, and fall
;; back to the below heuristics otherwise.
;; One additional note: currently, the files are named after the ASDF
;; system. We could think of using the (nicer) release name, on the condition
;; that only one system per release is documented, as it is the case
;; now. Later on, we could have a release entry with several manuals in it.
(defun primary-system (release)
  "Return RELEASE's primary system.
The primary system is determined heuristically as follows:
- it's the only system,
- it has the same name as the release,
- it has the same name as the release, with \"cl-\" prefixed removed,
- it has the shortest name."
  (let ((release-name (ql-dist:name release))
	(systems (ql-dist:provided-systems release)))
    (or (when (= (length systems) 1) (car systems))
	(find release-name systems :key #'ql-dist:name :test #'string=)
	(find (trim release-name) systems :key #'trim :test #'string=)
	(first (sort systems
		     (lambda (name1 name2) (< (length name1) (length name2)))
		     :key #'ql-dist:name)))))

(defgeneric library-name (library)
  (:documentation "Return LIBRARY's name.")
  (:method ((system-name string))
    "Return ASDF SYSTEM-NAME."
    system-name)
  (:method ((system ql-dist:system))
    "Return Quicklisp SYSTEM's name."
    (ql-dist:name system))
  (:method ((release ql-dist:release))
    "Return Quicklisp RELEASE's primary system's name."
    (library-name (primary-system release))))



;; ==========================================================================
;; General File System Utilities
;; ==========================================================================

(defun pathnames-matching (pattern directory)
  "Return a list of all pathnames matching PATTERN in DIRECTORY."
  (directory (merge-pathnames pattern (truename directory))))

(defun *.* (directory)
  "Return a list of all pathnames matching *.* in DIRECTORY."
  (pathnames-matching "*.*" directory))

(defun *.log (directory)
  "Return a list of all pathnames matching *.log in DIRECTORY."
  (pathnames-matching "*.log" directory))

(defun *.html (directory)
  "Return a list of all pathnames matching *.html in DIRECTORY."
  (pathnames-matching "*.html" directory))

(defun renew-directory (directory)
  "Either create DIRECTORY, or remove everything from it."
  (when (probe-file directory)
    (sb-ext:delete-directory directory :recursive t))
  (ensure-directories-exist directory)
  (values))

(defun file-contents (file)
  "Attempt to safely read FILE into a string and return it.
Safely means try to detect a proper encoding."
  (handler-case (uiop:read-file-string file)
    (error ()
      (handler-case (uiop:read-file-string file :external-format :latin-1)
	(error ()
	  (uiop:read-file-string file
	     :external-format '(:utf-8 :replacement #\?)))))))



;; ==========================================================================
;; Quickref Distribution Utilities
;; ==========================================================================

(defvar *top-directory* (asdf:system-source-directory :fr.epita.lrde.quickref)
  "Quickref's top directory.")

(defvar *share-directory* (merge-pathnames #p"share/" *top-directory*)
  "Quickref's share directory.")

(defvar *css-directory* (merge-pathnames #p"css/" *share-directory*)
  "Quickref's CSS directory.")

(defvar *templates-directory* (merge-pathnames #p"templates/" *share-directory*)
  "Quickref's templates directory.")

(defvar *bin-directory* (merge-pathnames #p"bin/" *top-directory*)
  "Quickref's bin directory.")



;; ==========================================================================
;; Quickref Output Utilities
;; ==========================================================================

;; #### NOTE: *OUTPUT-DIRECTORY* is a user-level parameter, so everything that
;; #### depends on it is computed dynamically through functions, instead of
;; #### being set by variables.

(defun logs-directory ()
  "Return Quickref's logs directory."
  (merge-pathnames #p"logs/" *output-directory*))

(defun declt-logs-directory ()
  "Return quickref's Declt logs directory."
  (merge-pathnames #p"declt/" (logs-directory)))

(defun declt-log-file (system-name)
  "Return Quickref's Declt log file for ASDF SYSTEM-NAME."
  (merge-pathnames (make-pathname :name system-name :type "log")
		   (declt-logs-directory)))

(defun makeinfo-logs-directory ()
  "Return quickref's Makeinfo logs directory."
  (merge-pathnames #p"makeinfo/" (logs-directory)))

(defun makeinfo-log-file (system-name)
  "Return Quickref's Makeinfo log file for ASDF SYSTEM-NAME."
  (merge-pathnames (make-pathname :name system-name :type "log")
		   (makeinfo-logs-directory)))

(defun texinfo-directory ()
  "Return Quickref's generated Texinfo files directory."
  (merge-pathnames #p"texinfo/" *output-directory*))

(defun texinfo-file (system-name)
  "Return ASDF SYSTEM-NAME's Texinfo file."
  (merge-pathnames (make-pathname :name system-name :type "texi")
		   (texinfo-directory)))

(defun html-directory ()
  "Return Quickref's generated HTML files directory."
  (merge-pathnames #p"html/" *output-directory*))

(defun html-file (name)
  "Return NAME.html file pathname."
  (merge-pathnames (make-pathname :name name :type "html") (html-directory)))

(defun cache-directory ()
  "Return Quickref's compilation cache root directory.
This is the location used when the cache policy is local. ASDF and Declt
get their own subdirectory, every documented system also has its own one,
leading to duplicate compilation of every shared dependency."
  (merge-pathnames #p"cache/" *output-directory*))


(defun ensure-quickref-directory (&key declt makeinfo)
  "Make sure we have fresh DECLT and/or MAKEINFO directories.
This function handles the cache, log and output directories at the same time."
  (when declt
    (when (and *empty-cache* (eq *cache-policy* :local))
      (renew-directory (cache-directory)))
    (renew-directory (declt-logs-directory))
    (renew-directory (texinfo-directory)))
  (when makeinfo
    (renew-directory (makeinfo-logs-directory))
    (renew-directory (html-directory))))

;;; util.lisp ends here
