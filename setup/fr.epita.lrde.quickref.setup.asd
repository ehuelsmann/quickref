;;; fr.epita.lrde.quickref.setup.asd --- ASDF system definition

;; Copyright (C) 2018, 2019 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:defsystem :fr.epita.lrde.quickref.setup
  :long-name "Quickref Setup"
  :description "Quickref's setup library"
  :long-description "\
Quickref's setup library provides support for various preload configuration
parameters. For a more complete description of Quickref, see the
fr.epita.lrde.quickref system."
  :author "Didier Verna"
  :maintainer ("Antoine Martin" "Didier Verna")
  :mailto "quickref-devel@common-lisp.net"
  :license "BSD"
  :components ((:file "setup")))

(asdf:defsystem :fr.epita.lrde.quickref.setup/parallel
  :long-name "Quickref, parallel setup"
  :description "Quickref's automatic configuration of parallel support"
  :long-description "\
This is a virtual subsystem or Quickref (no actual code). Its purpose is only
to autodetect parallel support and update Quickref's preload configuration on
load. For a more complete description of Quickref, see the
fr.epita.lrde.quickref system."
  :author "Didier Verna"
  :maintainer ("Antoine Martin" "Didier Verna")
  :mailto "didier@didierverna.net"
  :license "BSD"
  :depends-on (:fr.epita.lrde.quickref.setup)
  :perform (load-op (o c)
	     (declare (ignore o c))
	     (call-function "fr.epita.lrde.quickref.setup:setup-parallel")))

;;; fr.epita.lrde.quickref.setup.asd ends here
