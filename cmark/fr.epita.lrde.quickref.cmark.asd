;;; fr.epita.lrde.quickref.cmark.asd --- ASDF system definition

;; Copyright (C) 2018 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Antoine Martin and Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:load-system :fr.epita.lrde.quickref.setup)

(asdf:defsystem :fr.epita.lrde.quickref.cmark
  :long-name "CommonMark Foreign Function Interface"
  :description "Bindings to libcmark"
  :long-description "\
CMark provides bindings to libcmark, the C implementation of CommonMark.
CommonMark[1] is itself an attempt at properly specifying the MarkDown[2]
format.

[1] http://commonmark.org/
[2] https://daringfireball.net/projects/markdown/"
  :author "Antoine Martin"
  :maintainer ("Antoine Martin" "Didier Verna")
  :mailto "quickref-devel@common-lisp.net"
  :license "BSD"
  :version #.(fr.epita.lrde.quickref.setup:version :short)
  :depends-on (:fr.epita.lrde.quickref.setup :cffi)
  :serial t
  :components ((:file "cmark")))

;;; fr.epita.lrde.quickref.cmark.asd ends here
